import 'dart:core';

class App {
  late String name;
  late String sector;
  late String developer;
  late int year;

  void ShowOutPut() {
    print("Name of the App: $name");
    print("The category in which the App falls into: $sector");
    print("The application was developed by: $developer");
    print("The year in which the app won: $year");
  }

//function inside the class to capitalise all letters of the name
  String Caps(String Capname) {
    Capname = this.name.toUpperCase();
    return Capname;
  }
}

void main() {
  App App1 = App();

  App1.name = "Ambani Africa";
  App1.sector = "Best Educational Solution";
  App1.developer = "Ambani Africa";
  App1.year = 2021;
  String capital = App1.Caps(App1.name);

  App1.ShowOutPut();
//printing the name of the winner in capital letters
  print("\nThe name of the winning in capital letters: $capital");
}
